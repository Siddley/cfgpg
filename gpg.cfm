
<cflock timeout="90" throwontimeout="YES" type="EXCLUSIVE">

<!---
Version 1.3

Changes:
1.3.0 - The tag now lists [secret] keys and deletes secret keys
1.2.1 - moved the lock to single thread the whole tag
1.2.0 - The tag now accepts various ways of setting key ID
1.1.1 - added some output parameters for decrypt and verify
1.1.0 - fixed the file name error while signing
1.0.9 - fixed the temp file name to avoid conflicts
1.0.8 - upgraded to work with GPG 1.4.0 (file names for encrypt and sign)
1.0.7 - changed timeout settings
1.0.6 - added cflock around executes to avoid file reading conflict
      - fixed the "not deleting .asc file bug"
1.0.5 - fixed encrypt immediately after import key error
1.0.4 - added support for Idea module (optional)
--->

<!--- Set the global parameters unix style --->
<cfset GPGPath = "/usr/bin/gpg"><!--- the full path to the GPG executable --->
<cfset GPGHomeDir = "/home/railo/gpg/keys"><!--- the default home directory for GPG keyrings --->
<cfset GPGTempDir = "/home/railo/gpg/temp"><!--- the directory for GPG temp files - will be created if it does not exist--->


<!--- Set the global parameters windows style
<cfset GPGPath = "c:\gnupg\gpg.exe"><!--- the full path to the GPG executable --->
<cfset GPGHomeDir = "c:\gnupg\keys"><!--- the default home directory for GPG keyrings --->
<cfset GPGTempDir = "c:\gnupg\temp"><!--- the directory for GPG temp files - will be created if it does not exist--->
 --->

<!--- <cfset GPGIdeaPath = "/home/railo/gpg/idea.dll">optional - comment out if no Idea support --->

<!---
********************************************
    don't change anything below here
********************************************
--->
<cfset caller.cfgpgversion = "cf_gpg Version 1.3">
<cfset GPGPath = REReplace(GPGPath,"\\","/","ALL")>
<cfset GPGHomeDir = REReplace(GPGHomeDir,"\\","/","ALL")>
<cfset GPGHomeDir = REReplace(GPGHomeDir,"/$","","ALL")>
<cfset GPGTempDir = REReplace(GPGTempDir,"\\$","","ALL")>

<cfif NOT Isdefined("GPGIdeaPath") OR
      Trim(GPGIdeaPath) EQ "">
  <cfset IdeaSupport = "">
<cfelse>
  <cfset IdeaSupport = '--load-extension "#GPGIdeaPath#"'>
</cfif>

<cfif IsDefined("attributes.keyringDirectory") AND attributes.keyringDirectory NEQ "">
  <cfset attributes.keyringDirectory = REReplace(attributes.keyringDirectory,"\\","/","ALL")>
  <cfset keyringDirectory = REReplace(attributes.keyringDirectory,"/$","","ALL")>
<cfelse>
  <cfset keyringDirectory = GPGHomeDir >
</cfif>

<cfset GPGHomeDir = keyringDirectory >
<cfset caller.gpgMessage = "">
<cfset optionsLine = '#IdeaSupport# --homedir "#GPGHomeDir#" --no-options --keyserver-options no-auto-key-retrieve --armor --always-trust --no-greeting '><!--- --logger-fd 1 --->
<cfparam name="attributes.commentLine" default="CFGPG - Siddley Inc. http://siddley.net">

<cfif NOT DirectoryExists("#GPGTempDir#")>
  <cfdirectory action="CREATE" directory="#GPGTempDir#">
</cfif>


<cfset FileName1 = GPGTempDir&"/"&Createuuid()&".tmp">
<cfset FileName2 = GPGTempDir&"/"&Createuuid()&".tmp">
<cfset FileName3 = GPGTempDir&"/"&Createuuid()&".tmp">


<cfscript>
//correct full headers for display
function fixGPGheaders( GPGFixContent ){
    Return ReplaceNoCase(GPGFixContent, "(MingW32)" ,
                  "#chr(13)##chr(10)#Comment: #attributes.commentLine#");
    }
</cfscript>

<!--- First check for general required attributes --->
<cfif NOT IsDefined("attributes.action")>
  <cfthrow message="'ACTION' is a required attribute">
</cfif>

<cfswitch expression="#attributes.action#">

<!--- IMPORT A KEY --->

  <cfcase value="import">
    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.key")>
      <cfthrow message="'KEY' is a required attribute">
    </cfif>
    <!--- First write the key to a file --->
    <cffile action="WRITE" file="#FileName1#" output="#attributes.key#" addnewline="No">
    <!--- now set the command line --->
    <cfset commandLine = "#optionsLine# --logger-fd 1 --import #FileName1#">
    <!--- now make GPG do it's stuff --->

      <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName2#" timeout="60"/>

    <!--- read the result --->
    <cffile action="READ" file="#FileName2#" variable="gpgResult">
    <cfset caller.gpgMessage = gpgResult>
    <!--- parse the result to check success --->
    <cfif REFindNocase("^gpg: key ",Trim(gpgResult))>
      <!--- import was successful, send back the keyID --->
      <cfset caller.nameout = GetToken(gpgResult,2,"""") >
      <cfset caller.gpgout = Replace(GetToken(gpgResult,3," "),":","","ALL")>
    <cfelse>
      <cfset caller.gpgout = "0">
    </cfif>
  </cfcase>

<!--- EXPORT A KEY --->

  <cfcase value="export">
    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.keyID")>
      <cfthrow message="'KEYID' is a required attribute">
    </cfif>
    <!--- Set the command line --->
    <cfset commandLine = '#optionsLine#  --export "#attributes.keyID#"'>
    <!--- Now make GPG do it's stuff --->

      <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName1#" timeout="60"/>

    <!--- read the result --->
    <cffile action="READ" file="#FileName1#" variable="gpgResult">
    <cfset caller.gpgMessage = fixGPGheaders(gpgResult)>
    <!--- parse the result to check success --->
    <cfif NOT REFindNocase("gpg: WARNING:",Trim(gpgResult))>
      <!--- export was successful, send back the key --->
      <cfset caller.gpgout = fixGPGheaders(gpgResult)>
    <cfelse>
      <cfset caller.gpgout = "0">
    </cfif>
  </cfcase>

<!--- DELETE A PUBLIC KEY --->

  <cfcase value="delete-key">
    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.keyID")>
      <cfthrow message="'KEYID' is a required attribute">
    <cfelse>
      <cfset attributes.keyID = reReplaceNoCase(attributes.keyID, " +", "", "ALL") >
    </cfif>
    <!--- Set the command lines --->
    <cfset commandLine = '#optionsLine# --logger-fd 1 --batch  --yes --delete-key "#attributes.keyID#"'>
    <!--- Now make GPG do it's stuff --->

      <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName1#" timeout="60"/>

    <!--- read the result --->
    <cffile action="READ" file="#FileName1#" variable="gpgResult">
    <cfset caller.gpgMessage = gpgResult>
    <!--- parse the result to check success --->
    <cfif NOT REFindNocase("delete key failed:",Trim(gpgResult))
          AND NOT REFindNocase("delete-secret-keys",Trim(gpgResult))    >
      <!--- delete was successful --->
      <cfset caller.gpgout = "1">
    <cfelse>
      <cfset caller.gpgout = "0">
    </cfif>
  </cfcase>

<!--- DELETE A SECRET KEY --->

  <cfcase value="delete-secret-key">
    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.keyID")>
      <cfthrow message="'KEYID' is a required attribute">
    <cfelse>
      <cfset attributes.keyID = reReplaceNoCase(attributes.keyID, " +", "", "ALL") >
    </cfif>
    <!--- Set the command lines --->
    <cfset commandLine = '#optionsLine# --logger-fd 1 --batch  --yes --delete-secret-key "#attributes.keyID#"'>
    <!--- Now make GPG do it's stuff --->

      <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName1#" timeout="60"/>

    <!--- read the result --->
    <cffile action="READ" file="#FileName1#" variable="gpgResult">
    <cfset caller.gpgMessage = gpgResult>
    <!--- parse the result to check success --->
    <cfif NOT REFindNocase("delete key failed:",Trim(gpgResult)) >
      <!--- delete was successful --->
      <cfset caller.gpgout = "1">
    <cfelse>
      <cfset caller.gpgout = "0">
    </cfif>
  </cfcase>

<!--- SIGN A MESSAGE --->

  <cfcase value="sign">

    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.SignKeyID")>
      <cfthrow message="'SIGNKEYID' is a required attribute">
    </cfif>
    <cfif NOT IsDefined("attributes.subject")>
      <cfthrow message="'SUBJECT' is a required attribute">
    <cfelseif NOT Len(attributes.subject)>
      <cfthrow message="'SUBJECT' must not be a zero length string'">
    </cfif>
    <!--- First write the subject message to a file --->
    <cffile action="WRITE" file="#FileName1#" output="#attributes.subject#" addnewline="No">
    <!--- Set the command lines --->
    <cfset commandLine = '#optionsLine# --logger-fd 1 --batch  --default-key "#attributes.SignKeyID#" --clearsign #FileName1#'>
    <!--- Now make GPG do it's stuff --->
    <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName2#" timeout="60"/>
    <!--- read the results --->
    <cffile action="READ" file="#FileName2#" variable="gpgResult">
    <cfset caller.gpgMessage = gpgResult>
    <!--- fix the filename --->
    <cfset ascFilename = Replace(FileName1,"tmp","tmp.asc")>
    <!--- parse the result to check success --->
    <cfif Len(Trim(gpgResult))>
      <!--- Signing failed --->
      <cfset caller.gpgout = "0">
    <cfelse>
      <cffile action="READ" file="#ascFilename#" variable="gpgResult">
      <cfset caller.gpgout = fixGPGheaders(gpgResult)>
    </cfif>
    <cftry>
      <cffile action="DELETE" file="#ascFilename#">
      <cfcatch type="Any"></cfcatch>
    </cftry>
  </cfcase>

<!--- ENCRYPT A MESSAGE --->

  <cfcase value="encrypt">
    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.EncryptKeyID")>
      <cfthrow message="'EncryptKeyID' is a required attribute">
    </cfif>
    <cfif NOT IsDefined("attributes.subject")>
      <cfthrow message="'SUBJECT' is a required attribute">
    <cfelseif NOT Len(attributes.subject)>
      <cfthrow message="'SUBJECT' must not be a zero length string'">
    </cfif>
    <!--- First write the subject message to a file --->
    <cffile action="WRITE" file="#FileName1#" output="#attributes.subject#" addnewline="No">
    <!--- Set the command lines --->
    <cfif IsDefined("attributes.SignKeyID")
          AND Trim(attributes.SignKeyID) NEQ "">
      <cfset commandLine = '#optionsLine# --logger-fd 1 --batch  --local-user "#attributes.SignKeyID#" --recipient "#attributes.EncryptKeyID#" --sign --encrypt #FileName1#'>
    <cfelse>
      <cfset commandLine = '#optionsLine# --batch  --recipient "#attributes.EncryptKeyID#"  --encrypt #FileName1#'>
    </cfif>
    <!--- Now make GPG do it's stuff --->

      <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName2#" timeout="60"/>

    <!--- read the results --->
    <cffile action="READ" file="#FileName2#" variable="this.gpgMessage">
    <cfset caller.gpgMessage = this.gpgMessage>
    <cftry>
      <cfset ascFilename = Replace(FileName1,"tmp","tmp.asc") >
      <cffile action="READ" file="#ascFilename#" variable="gpgEncryptResult">
    <cfcatch type="Any"><cfset gpgEncryptResult = 0></cfcatch>
    </cftry>
    <!--- parse the result to check success --->
    <cfset regex = "^(-----BEGIN PGP MESSAGE-----){1}.*(-----END PGP MESSAGE-----){1}$">
    <cfif NOT REFindNoCase(regex,Trim(gpgEncryptResult))>
      <!--- Encrypting failed --->
      <cfset caller.gpgout = "0">
      <cftry><cffile action="DELETE" file="#ascFilename#">
      <cfcatch type="Any"></cfcatch>
      </cftry>
    <cfelse>
      <!--- fix the filename --->
      <cfset caller.gpgout = fixGPGheaders(gpgEncryptResult)>
      <cffile action="DELETE" file="#ascFilename#">
    </cfif>
  </cfcase>

<!--- DECRYPT A MESSAGE --->

  <cfcase value="decrypt">
    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.subject")>
      <cfthrow message="'SUBJECT' is a required attribute">
    <cfelseif NOT Len(attributes.subject)>
      <cfthrow message="'SUBJECT' must not be a zero length string'">
    </cfif>
    <!--- First write the subject message to a file --->
    <cffile action="WRITE" file="#FileName1#" output="#attributes.subject#" addnewline="No">
    <!--- Set the command lines --->
    <cfset commandLine = '#optionsLine# --logger-fd 1 --batch --try-all-secrets --output #FileName3# --decrypt #FileName1#'>
    <!--- Now make GPG do it's stuff --->

      <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName2#" timeout="60"/>

    <!--- read the results --->
    <cffile action="READ" file="#FileName2#" variable="gpgResult">
    <cfset caller.gpgMessage = gpgResult>
    <!--- parse the result to check success --->
    <cfif REFindNoCase("gpg:\s+decryption failed:", gpgResult)>
      <!--- decrypt failed --->
      <cfset caller.gpgout = 0>
      <cfset caller.gpgSigOut = 0>
      <cfset caller.SignNameOut = "">
      <cfset caller.SignIDOut = "">
      <cfset caller.SignTimeOut = "">
    <cfelse>
      <cffile action="READ" file="#Filename3#" variable="gpgResult2">
      <cfset caller.gpgout = gpgResult2>
    </cfif>
      <cfif REFindnocase("(gpg: Signature){1}.*(gpg: Good)",gpgResult)>
        <cfset caller.gpgSigOut = 1>
        <cfset SignNameOut = REFindNoCase("gpg: +Good +signature +from +""(.*)""", gpgResult, 1, true)>
        <cfset caller.SignNameOut = Mid(gpgResult, SignNameOut.pos[2], SignNameOut.len[2])>
        <cfset SignIDTOut = REFindnocase("gpg: +Signature +made +(.*) +using.*ID +([^\s]+)",gpgResult, 1, true)>
        <cfset caller.SignIDOut = Mid(gpgResult, SignIDTOut.pos[3], SignIDTOut.len[3])>
        <cfset caller.SignTimeOut = Mid(gpgResult, SignIDTOut.pos[2], SignIDTOut.len[2])>
      </cfif>
  </cfcase>

<!--- VERIFY A MESSAGE --->

  <cfcase value="verify">
    <!--- First check for required attributes --->
    <cfif NOT IsDefined("attributes.subject")>
      <cfthrow message="'SUBJECT' is a required attribute">
    <cfelseif NOT Len(attributes.subject)>
      <cfthrow message="'SUBJECT' must not be a zero length string'">
    </cfif>
    <!--- First write the subject message to a file --->
    <cffile action="WRITE" file="#FileName1#" output="#attributes.subject#" addnewline="No">
    <!--- Set the command lines --->
    <cfset commandLine = '#optionsLine# --logger-fd 1 --batch  --verify #FileName1#'>
    <!--- Now make GPG do it's stuff --->

      <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName2#" timeout="60"/>

    <!--- read the results --->
    <cffile action="READ" file="#FileName2#" variable="gpgResult">
    <cfset caller.gpgMessage = gpgResult>
    <!--- parse the result to check success --->
    <cfif NOT REFindnocase("^(gpg: Signature){1}.*(gpg: Good)",Trim(gpgResult))>
      <!--- Verification failed --->
      <cfset caller.gpgout = "0">
    <cfelse>
        <cfset caller.gpgout = "1">
        <cfset SignNameOut = REFindNoCase("gpg: +Good +signature +from +""(.*)""", gpgResult, 1, true)>
        <cfset caller.SignNameOut = Mid(gpgResult, SignNameOut.pos[2], SignNameOut.len[2])>
        <cfset SignIDTOut = REFindnocase("gpg: +Signature +made +(.*) +using.*ID +([^\s]+)",gpgResult, 1, true)>
        <cfset caller.SignIDOut = Mid(gpgResult, SignIDTOut.pos[3], SignIDTOut.len[3])>
        <cfset caller.SignTimeOut = Mid(gpgResult, SignIDTOut.pos[2], SignIDTOut.len[2])>
    </cfif>
  </cfcase>

  <!--- LIST PUBLIC KEYS --->

    <cfcase value="list-public-keys">

      <!--- Set the command lines --->
      <cfset commandLine = '#optionsLine# --logger-fd 1 --fingerprint'>
      <!--- Now make GPG do it's stuff --->

        <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName2#" timeout="60"/>

      <!--- read the results --->
      <cffile action="READ" file="#FileName2#" variable="gpgResult">
      <cfset caller.gpgMessage = gpgResult>
      <cfset caller.gpgout = "">

      <!--- parse the result to check success --->
      <cfif Len(Trim(gpgResult)) AND NOT reFindNoCase("gpg:", gpgResult) >
        <!--- got something --->
        <cfset caller.gpgout = gpgResult>
      <cfelse>
        <cfset caller.gpgout = 0>
      </cfif>
    </cfcase>

    <!--- LIST SECRET KEYS --->

      <cfcase value="list-secret-keys">

        <!--- Set the command lines --->
        <cfset commandLine = '#optionsLine# --logger-fd 1 --list-secret-keys'>
        <!--- Now make GPG do it's stuff --->

          <cfexecute name="#GPGPath#" arguments="#commandLine#" outputfile="#FileName2#" timeout="60"/>

        <!--- read the results --->
        <cffile action="READ" file="#FileName2#" variable="gpgResult">
        <cfset caller.gpgMessage = gpgResult>

        <!--- parse the result to check success --->
        <cfif Len(Trim(gpgResult)) AND NOT reFindNoCase("gpg:", gpgResult)>
          <!--- got something --->
          <cfset caller.gpgout = gpgResult>
        <cfelse>
          <cfset caller.gpgout = 0>
        </cfif>
      </cfcase>

</cfswitch>

<cftry>
  <cffile action="DELETE" file="#FileName1#">
  <cffile action="DELETE" file="#FileName2#">
  <cffile action="DELETE" file="#FileName3#">
<cfcatch type="Any"></cfcatch>
</cftry>

</cflock>

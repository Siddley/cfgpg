cf_gpg - A Coldfusion Custom Tag (wrapper) for the GnuPG OpenPGP executable
---------------------------------------------------------------------------

To find out about GnuPG and to download the latest version, please see
http://www.gnupg.org/


CF_GPG INSTALL INSTRUCTIONS
---------------------------

Set default directory for the gnupg executable at the top of the gpg.cfm template
Set default directory for the keyrings at the top of the gpg.cfm template
All keys for signing or decrypting should have NO password (Careful!)

On windows you need at least the folowing files in the default executable directory:
gpg.exe
iconv.dll

If the following files are not in the default gpg executable directory, they
will be created the first time you run cf_gpg and the operation will fail. Simply
run cf_gpg again and from then on it will work correctly:
trustdb.gpg
random_seed
pubring.gpg
secring.gpg


GENERAL NOTES
--------------
If any function fails, gpgout will be 0
gpgmessage is output for every function and shows gpg logger messages


IMPORT A KEY
-------------

<cf_gpg action="import"
        keyringDirectory = "path to directory (no trailing slash)"
        key="Paste PGP key here">

Input:
keyringDirectory - optional, defaults to gpg home directory

Output:
gpgout - the key ID (0xXXXXXXXX)
nameout - The key name (your name <your_name@someplace.com>)


EXPORT A KEY
-------------

<cf_gpg action="export"
        keyringDirectory = "path to directory (no trailing slash)"
        keyID="Paste PGP key ID here"
        commentLine="the comment you want on the key">

Input:
KeyID - is either the key ID (0xXXXXXXXX) or the e-mail address (your_name@someplace.com)
keyringDirectory - [optional] defaults to gpg home directory
CommentLine - [optional] Default is "CFGPG - Siddley Inc. http://siddley.net"

Output:
gpgout - the key


DELETE A PUBLIC KEY
-------------------
Note: this will not delete a key pair. If there is a secret key this will fail and nothing will happen.

<cf_gpg action="delete-key"
        keyringDirectory = "path to directory (no trailing slash)"
        keyID="Paste PGP key ID here">

Input:
KeyID - is either the key ID (0xXXXXXXXX) or the e-mail address (your_name@someplace.com)
keyringDirectory - [optional] defaults to gpg home directory

Output:
gpgout - 1 if successful, 0 if failed

DELETE A SECRET KEY
-------------------
Note: this will not delete a key pair. If there is a public key it will remain.

<cf_gpg action="delete-secret-key"
        keyringDirectory = "path to directory (no trailing slash)"
        keyID="Paste PGP key ID here">

Input:
KeyID - Must be the full key fingerprint
keyringDirectory - [optional] defaults to gpg home directory

Output:
gpgout - 1 if successful, 0 if failed


CLEARSIGN A MESSAGE
--------------------

<cf_gpg action="sign"
        SignKeyID="Paste PGP key ID here"
        keyringDirectory = "path to directory (no trailing slash)"
        subject="your message here"
        commentLine="the comment you want">

Input:
SignKeyID - is either the key ID (0xXXXXXXXX) or the e-mail address
            (your_name@someplace.com) for the signing key.
Subject - is the message to be clearsigned
keyringDirectory - [optional] defaults to gpg home directory
CommentLine - [optional] Default is "CFGPG - Siddley Inc. http://siddley.net"

Output:
gpgout - the clearsigned message


ENCRYPT and SIGN A MESSAGE
---------------------------

<cf_gpg action="encrypt"
        EncryptKeyID="as before"
        SignKeyID="as before"
        keyringDirectory = "path to directory (no trailing slash)"
        subject="your message here"
        commentLine="the comment you want">

Input:
SignKeyID - [optional] is either the key ID (0xXXXXXXXX) or the e-mail address (your_name@someplace.com)
            for the signing key.
EncryptKeyID - is either the key ID (0xXXXXXXXX) or the e-mail address (your_name@someplace.com)
               for the encrypting key.
Subject - is the message to be signed and encrypted
keyringDirectory - [optional] defaults to gpg home directory
CommentLine - [optional] Default is "CFGPG - Siddley Inc. http://siddley.net"

Output:
gpgout - the encrypted message


DECRYPT and VERIFY A MESSAGE
-----------------------------

<cf_gpg action="decrypt"
        keyringDirectory = "path to directory (no trailing slash)"
        subject="your encrypted message here">

Input:
Subject - is the message to be decrypted and verified
keyringDirectory - [optional] defaults to gpg home directory

Output:
gpgout - the decrypted message
SignIDOut - The signing key ID if verification successful
SignNameOut - The signing key name if verification successful


VERIFY A MESSAGE
----------------

<cf_gpg action="verify"
        keyringDirectory = "path to directory (no trailing slash)"
        subject="your signed message here">

Input:
Subject - is the message to be verified
keyringDirectory - [optional] defaults to gpg home directory

Output:
gpgout - 0 if failed, 1 if verified OK
SignIDOut - the signing KeyID if verification successful
SignNameOut - The signing key name if verification successful

LIST PUBLIC KEYS
----------------

<cf_gpg action="list-public-keys"
        keyringDirectory = "path to directory (no trailing slash)">

Input:
keyringDirectory - [optional] defaults to gpg home directory

Output:
gpgout - the list if successful, otherwise empty


=========================================================================
cf_gpg - A Coldfusion Custom Tag (wrapper) for the GnuPG OpenPGP executable
Copyright (C) 2012 Siddley

This program is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see http://www.gnu.org/licenses/

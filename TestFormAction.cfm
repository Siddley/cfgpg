
<cfswitch expression="#form.action#">
  <cfcase value="import">
    <cf_gpg action="#form.action#"
            key="#form.message#"
            keyringDirectory = "#form.keyringDirectory#">
  </cfcase>
  <cfcase value="export,delete-key,delete-secret-key">
    <cf_gpg action="#form.action#"
            keyID="#form.keyID#"
            keyringDirectory = "#form.keyringDirectory#">
  </cfcase>
  <cfcase value="encrypt,decrypt,verify,sign">
    <cf_gpg action="#form.action#"
        signkeyID="#form.skeyID#"
        keyringDirectory = "#form.keyringDirectory#"
        encryptkeyID="#form.ekeyID#"
        subject="#form.message#">
  </cfcase>
  <cfcase value="list-public-keys,list-secret-keys">
    <cf_gpg action="#form.action#"
            keyringDirectory = "#form.keyringDirectory#">
  </cfcase>
  <cfdefaultcase><h3>No action selected!</h3><cfabort></cfdefaultcase>
</cfswitch>


<cfoutput>
<cfparam name="gpgSigOut" default="">
<cfparam name="nameout" default="">
<cfparam name="SignIDOut" default="">
<cfparam name="SignNameOut" default="">
<cfparam name="SignTimeOut" default="">
<pre>gpgout = #gpgout#</pre>
<pre>gpgsigout = #gpgsigout#</pre>
<pre>nameout = #HTMLCodeFormat(nameout)#</pre>
<pre>SignIDOut = #HTMLCodeFormat(SignIDOut)#</pre>
<pre>SignTimeOut = #SignTimeOut#</pre>
<pre>SignNameOut = #HTMLCodeFormat(SignNameOut)#</pre><br><br>
gpgMessage = <h3>#HTMLCodeFormat(gpgMessage)#</h3>
</cfoutput>

